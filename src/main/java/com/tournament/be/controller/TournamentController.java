package com.demo.example.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.example.exception.ResourceNotFoundException;
import com.demo.example.model.Tournament;
import com.demo.example.repository.TournamentRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1/")
public class TournamentController {

	@Autowired
	private TournamentRepository tournamentRepository;
	
	// get all tournaments
	@GetMapping("/tournaments")
	public List<Tournament> getAllTournaments(){
		return tournamentRepository.findAll();
	}		
	
	// create torunament rest api
	@PostMapping("/tournaments")
	public Tournament createTournament(@RequestBody Tournament tournament) {
		return tournamentRepository.save(tournament);
	}
	
	// get torunament by id rest api
	@GetMapping("/tournaments/{id}")
	public ResponseEntity<Tournament> getTournamentById(@PathVariable Long id) {
		Tournament tournament = tournamentRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Tournament not exist with id :" + id));
		return ResponseEntity.ok(tournament);
	}
	
	// update tournament rest api
	@PutMapping("/tournaments/{id}")
	public ResponseEntity<Tournament> updateTournament(@PathVariable Long id, @RequestBody Tournament tournamentDetails){
		Tournament tournament = tournamentRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Employee not exist with id :" + id));
		
		tournament.setYear(tournamentDetails.getYear());
		tournament.setType(tournamentDetails.getType());
		tournament.setTeams(tournamentDetails.getTeams());
		tournament.setScore(tournamentDetails.getScore());
		
		Tournament updatedTournament = tournamentRepository.save(tournament);
		return ResponseEntity.ok(updatedTournament);
	}
	
	// delete tournament rest api
	@DeleteMapping("/tournaments/{id}")
	public ResponseEntity<Map<String, Boolean>> deleteTournament(@PathVariable Long id){
		Tournament tournament = tournamentRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Tournament not exist with id :" + id));
		
		tournamentRepository.delete(tournament);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return ResponseEntity.ok(response);
	}
}
