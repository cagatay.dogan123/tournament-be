package com.demo.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.example.model.Tournament;

@Repository
public interface TournamentRepository extends JpaRepository<Tournament, Long>{

}
