package com.demo.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.example.model.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>{

}
