package com.demo.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.example.model.Team;

@Repository
public interface TeamRepository extends JpaRepository<Team, Long>{

}
