package com.demo.example.model;

import jakarta.persistence.*;

@Entity
@Table(name = "employees")
public class Employee {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "user_name")
	private String userName;

	@Column(name = "full_name")
	private String fullName;
	
	@Column(name = "role")
	private String role;

	@Column(name = "player_number")
	private String playerNumber;

	@Column(name = "age")
	private String age;
	
	public Employee() {
		
	}
	
	public Employee(String userName, String fullName, String role, String playerNumber, String age) {
		super();
		this.userName = userName;
		this.fullName = fullName;
		this.role = role;
		this.playerNumber = playerNumber;
		this.age = age;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getPlayerNumber() {
		return playerNumber;
	}
	public void setPlayerNumber(String playerNumber) {
		this.playerNumber = playerNumber;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
}
