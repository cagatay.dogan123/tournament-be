package com.demo.example.model;

import jakarta.persistence.*;

@Entity
@Table(name = "employees")
public class Team {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "name")
	private String name;

	@Column(name = "leader")
	private Employee leader;
	
	@Column(name = "players")
	private Employee players[];
	
	public Team() {
		
	}
	
	public Team(String name, Employee leader, Employee players[]) {
		super();
		this.name = name;
		this.leader = leader;
		this.players = players;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Employee getLeader() {
		return leader;
	}
	public void setLeader(Employee leader) {
		this.leader = leader;
	}
	public Employee[] getPlayers() {
		return players;
	}
	public void setPlayers(Employee players[]) {
		this.players = players;
	}
}
