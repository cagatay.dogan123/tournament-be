package com.demo.example.model;

import jakarta.persistence.*;

@Entity
@Table(name = "tournaments")
public class Tournament {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "year")
	private String year;

	@Column(name = "type")
	private String type;
	
	@Column(name = "teams")
	private Team[] teams;

	@Column(name = "score")
	private String score;
	
	public Tournament() {
		
	}
	
	public Tournament(String year, String type, Team[] teams, String score) {
		super();
		this.year = year;
		this.type = type;
		this.teams = teams;
		this.score = score;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Team[] getTeams() {
		return teams;
	}
	public void setTeams(Team[] teams) {
		this.teams = teams;
	}
	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}
}
